import $ from 'jquery'
import M from 'materialize-css/dist/js/materialize'

$(() => {

    let checkGeoOp = setInterval(() => {
        if ($('#geoop-form-custom-title-submitting').length) {
            themeGeoop()

            clearInterval(checkGeoOp)
        }

    }, 100)
})

function themeGeoop() {
    styleRequestType()
    styleRequestDetails()
    styleTextFields()
    stylePostcode()
    styleDateTime()
    styleSubmit()

    finalizeFormFields()
    addIcons()
    makeResponsive()

    M.FormSelect.init($('#request_type'))
    M.Datepicker.init($('#preferred_date'), {
        container: $('body'),
        format: 'dd mmm yyyy'
    })
    M.Timepicker.init($('#preferred_time'), {
        container: 'body'
    })

    $('label').click((e) => {
        if ($(e.target).parent().find('select').length) return

        $(e.target).parent().find($(e.target).parent().find('textarea').length ? 'textarea' : 'input').focus()
    })

}


function finalizeFormFields() {
    $('.geoop-form-table-value').each((i, el) => {
        $(el).children().wrapAll($('<div class="input-field col s12" />'))

        $(el).find('.input-field').addClass(`
        ${$(el).find('input, textarea').first().attr('id')
        ? $(el).find('input, textarea').first().attr('id')
        : $(el).find('select').attr('id')
        }__wrapper`)
    })

    $('#geoop-form-placeholder').find('.input-field').not('.geoop-form-postcode__wrapper, .geoop-form-job-start-date__wrapper, .geoop-form-job-start-time__wrapper').each((i, el) => {
        $(el).unwrap()
    })
    $('.geoop-form-table-name').remove()
    $('#geoop-form-placeholder').find('.input-field').not('.geoop-form-postcode__wrapper, .geoop-form-job-start-date__wrapper, .geoop-form-job-start-time__wrapper').each((i, el) => {
        $(el).unwrap()
    })

    $('#geoop-form-placeholder').find('tbody').unwrap().contents().unwrap()
    $('#geoop-form-placeholder').find('form').addClass('col s12')
    $('#geoop-form-placeholder').wrap('<div id="geoop-form-wrapper" class="geoop-form-wrapper row z-depth-5 modal close"></div>')

    // MODALIZATION


    M.Modal.init($('#geoop-form-wrapper'), {
        'startingTop': '10%',
        'onOpenStart': () => $('header').hide(250),
        'onCloseStart': () => $('header').show(250)
    })

    $('.geoop-form-button').click(() => {
        M.Modal.getInstance($('#geoop-form-wrapper')[0]).open()
    })

    $('#geoop-form-placeholder').find('.geoop-form-request-type__wrapper, .geoop-form-request-details__wrapper, .geoop-form-address-1__wrapper, .geoop-form-address-2__wrapper').wrap('<div class = "row" />')
    $('#geoop-form-placeholder').find('.geoop-form-first-name__wrapper, .geoop-form-last-name__wrapper').wrapAll('<div class = "row" />')
    $('#geoop-form-placeholder').find('.geoop-form-phone__wrapper, .geoop-form-email__wrapper').wrapAll('<div class = "row" />')
    $('#geoop-form-placeholder').find('.geoop-form-city__wrapper, .geoop-form-stpr__wrapper, .geoop-form-postcode__wrapper').wrapAll('<div class = "row" />')
    $('#geoop-form-placeholder').find('.geoop-form-job-start-date__wrapper, .geoop-form-job-start-time__wrapper').wrapAll('<div class = "row" />')
}

function styleRequestType() {

    $('#geoop-form-request-type').prepend($(`<option
        id = "type",
        value = "",
        disabled
        selected
    />`).text($('#geoop-form-custom-title-request-type').text()))

    M.FormSelect.init($('#geoop-form-request-type'))
}

function styleRequestDetails() {
    $('#geoop-form-request-details').addClass('materialize-textarea')

    $('#geoop-form-request-details').after($('<label for="geoop-form-request-details"/>').text($('#geoop-form-custom-title-request-details').text()))
}

function stylePostcode() {
    $('#geoop-form-placeholder').find('#geoop-form-postcode').detach()
        .wrap('<div class="input-field col s12 geoop-form-postcode__wrapper"/>')
        .parent().insertAfter($('#geoop-form-stpr').parent().parent())
    $('#geoop-form-placeholder').find('#geoop-form-postcode')
        .after(
            $(`<label for="geoop-form-postcode"/>`)
            .text($('#geoop-form-placeholder').find('#geoop-form-postcode').attr('placeholder')))
    $('#geoop-form-placeholder').find('#geoop-form-postcode')
        .attr('placeholder', '')
}

function styleDateTime() {
    $('#geoop-form-placeholder').find('#geoop-form-job-start-date').attr('placeholder', 'Preferred Date').addClass('datepicker')
    $('#geoop-form-placeholder').find('#geoop-form-job-start-date').parent().unwrap().contents().unwrap()
    $('#geoop-form-placeholder').find('#geoop-form-job-start-date').wrap('<div class="input-field col s12 geoop-form-job-start-date__wrapper" />')

    $('.geoop-form-job-start-date__wrapper').after(
        $('<div class="input-field col s12 geoop-form-job-start-time__wrapper" />').append(
            $('<input class="timepicker" placeholder="Preferred Time" />')))

    M.Datepicker.init($('#geoop-form-placeholder').find('#geoop-form-job-start-date'), {
        container: $('body'),
        format: 'dd mmm yyyy',
    })
    M.Timepicker.init($('#geoop-form-placeholder').find('.timepicker'), {
        container: 'body',
    })

    $('#geoop-form-placeholder').find('.timepicker').change((e) => {
        let time = $(e.target).val()

        let tempHour = parseInt(time.split(':')[0]) + (time.split(' ')[1] == 'AM' ? 0 : 12)
        let tempMinute = Math.round(parseInt(time.split(':')[1].split(' ')[0]) / 5) * 5
        $('#geoop-form-job-start-hour, #geoop-form-job-start-minute').find('option[selected="selected"]').removeAttr('selected')
        $('#geoop-form-job-start-hour').find(`option[value=${tempHour}]`).attr('selected', 'selected')
        $('#geoop-form-job-start-minute').find(`option[value=${tempMinute == 60 ? 0 : tempMinute}]`).attr('selected', 'selected')
    })

    $('#geoop-form-job-start-hour, #geoop-form-job-start-minute').attr('type', 'hidden')
}

function styleSubmit() {
    $('#geoop-form-placeholder').find('#geoop-form-submit').parent().unwrap().contents().unwrap()
    $('#geoop-form-placeholder').find('#geoop-form-submit').addClass('btn waves-effect col s12').wrap('<div class="modal-footer" />')
    $('#geoop-form-placeholder').find('#geoop-form-custom-title-submitting').hide()
}

function styleTextFields() {
    $('#geoop-form-placeholder').find('input:not(:first, #geoop-form-postcode, #geoop-form-job-start-date, #geoop-form-job-start-time, #geoop-form-submit)').each((i, el) => {
        $(el).after(
            $(`<label for="${$(el).attr('id')}"/>`)
            .text(
                $(`#geoop-form-custom-title-${$(el).attr('id').split('geoop-form-')[1]}`).text()
            )
        )
    })
}

const icons = {
    'request-type': 'question_answer',
    'request-details': 'mode_edit',
    'first-name': 'person',
    'last-name': 'person_outline',
    'phone': 'phone',
    'email': 'email',
    'address-1': 'home',
    'address-2': 'add_location',
    'city': 'location_city',
    'stpr': 'location_on',
    'postcode': 'location_searching',
    'job-start-date': 'date_range',
    'job-start-time': 'access_time'
}

function addIcons() {
    for (let [name, icon] of Object.entries(icons)) {
        $('#geoop-form-placeholder').find(`.geoop-form-${name}__wrapper`).prepend($(`<i class="material-icons prefix">${icon}</i>`))
    }
}

function makeResponsive() {
    $('#geoop-form-placeholder').find('.geoop-form-phone__wrapper, .geoop-form-postcode__wrapper').addClass('m4')
    $('#geoop-form-placeholder').find('.geoop-form-first-name__wrapper,.geoop-form-last-name__wrapper, .geoop-form-job-start-date__wrapper, .geoop-form-job-start-time__wrapper').addClass('m6')
    $('#geoop-form-placeholder').find('.geoop-form-email__wrapper, .geoop-form-stpr__wrapper').addClass('m8')
    $('#geoop-form-placeholder').find('.geoop-form-city__wrapper, .geoop-form-stpr__wrapper').addClass('l4')

}