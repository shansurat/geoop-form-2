import $ from 'jquery'
import M from 'materialize-css/dist/js/materialize'

import form from './geoop-form.html'

const defaultOptions = {
    debug: false,

    customTitles: {
        'request-type': 'Request Type',
        'request-type-job': 'Job Request',
        'request-type-quote': 'Quote Request',
        'request-details': 'Request Details',
        'first-name': 'First Name',
        'last-name': 'Last Name',
        'phone': 'Phone',
        'email': 'Email',
        'address-1': 'Address',
        'address-2': 'Address 2',
        'city': 'City',
        'postcode': 'Postcode',
        'preferred-time': 'Preferred Time',
        'submit': 'Submit',
        'error-blank-fields': 'Some of the required fields in the form were left blank. Please fill out the highlighted fields before submitting the form.',
        'success': 'Your job request has been submitted successfully. Thank you!'
    }
}

let geoop_form = $.parseHTML(form)

let geoop = function (element) {
    return new geoop.prototype.init(element)
}

geoop.prototype.init = function (element) { //Initializing geoop()
    this.el = element
}

geoop.prototype.load = function (token, options, submitFunction) { // Initializing geoop().load()
    let $geoop_form = $(geoop_form).clone()

    let customTitles = defaultOptions.customTitles

    if (options && options.customTitles)
        Object.keys(options.customTitles).forEach(customTitle => {
            customTitles[customTitle] = options.customTitles[customTitle]
        });

    $geoop_form.find('#request_type__wrapper>label, #request_details__wrapper>#type').text(customTitles['request-type'])
    $geoop_form.find('#request_details__wrapper>#job').text(customTitles['request-type-job'])
    $geoop_form.find('#request_details__wrapper>#quote').text(customTitles['request-type-quote'])
    $geoop_form.find('#request_details__wrapper>label').text(customTitles['request-details'])
    $geoop_form.find('#first_name__wrapper>label').text(customTitles['first-name'])
    $geoop_form.find('#last_name__wrapper>label').text(customTitles['last-name'])
    $geoop_form.find('#phone__wrapper>label').text(customTitles['phone'])
    $geoop_form.find('#email__wrapper>label').text(customTitles['email'])
    $geoop_form.find('#address__wrapper>label').text(customTitles['address-1'])
    $geoop_form.find('#address2__wrapper>label').text(customTitles['address-2'])
    $geoop_form.find('#city__wrapper>label').text(customTitles['city'])
    $geoop_form.find('#postcode__wrapper>label').text(customTitles['postcode'])
    $geoop_form.find('#preferred_date').attr('placeholder', customTitles['preferred-date'])
    $geoop_form.find('#preferred_time').attr('placeholder', customTitles['preferred-time'])

    return new Promise((resolve, reject) => {
        $(() => {
            $(this.el).html($geoop_form)

            M.FormSelect.init($('#request_type'))
            M.Datepicker.init($('#preferred_date'), {
                container: $('body')
            })
            M.Timepicker.init($('#preferred_time'), {
                container: 'body'
            })

            $('label').click((e) => {
                if ($(e.target).parent().find('select').length) return

                $(e.target).parent().find($(e.target).parent().find('textarea').length ? 'textarea' : 'input').focus()
            })

            if (submitFunction)
                $(this.el).find('form#geoop').submit((e) => {
                    e.preventDefault()
                    let data = {}

                    $(e.target).serializeArray().forEach((el) => {
                        data[el.name] = el.value
                    })

                    submitFunction(data)
                })

            resolve($(this.el).find('#geoop-form'))
        })
    })
}

geoop.prototype.init.prototype = geoop.prototype

geoop.load = (token, options, submitFunction) => { // Initializing geoop.load()
    return geoop('#geoop-form-placeholder').load(token, options, submitFunction)
}

window.geoop = geoop