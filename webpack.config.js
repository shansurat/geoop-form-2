const autoprefixer = require('autoprefixer')
const path = require('path')

function tryResolve_(url, sourceFilename) {
    try {
        return require.resolve(url, {
            paths: [path.dirname(sourceFilename)]
        })
    } catch (e) {
        return ''
    }
}

function tryResolveScss(url, sourceFilename) {
    const normalizedUrl = url.endsWith('.scss') ? url : `${url}.scss`
    return tryResolve_(normalizedUrl, sourceFilename) ||
        tryResolve_(path.join(path.dirname(normalizedUrl), `_${path.basename(normalizedUrl)}`), sourceFilename)
}

module.exports = {
    entry: [
        './src/geoop-form/geoop-form.js',
        './src/geoop-form/geoop-form.scss',
        './src/geoop-form/geoop-form.html'
    ],
    output: {
        path: __dirname + '/dist/geoop-form',
        filename: 'geoop-form.js',
    },
    module: {
        rules: [{
                test: /\.scss$/,
                use: [{
                        loader: 'file-loader',
                        options: {
                            name: 'geoop-form.css',
                        },
                    },
                    {
                        loader: 'extract-loader'
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: () => [autoprefixer()],
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            includePaths: ['./node_modules']
                        }
                    }
                ]
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                },
            },
            {
                test: /\.(html)$/,
                use: {
                    loader: 'html-loader',
                    options: {
                        minimize: true,
                        interpolation: false
                    }
                }
            }
        ],
    },
}